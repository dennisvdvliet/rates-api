package main

import (
	"encoding/json"
	"github.com/gorilla/mux"
	"log"
	"net/http"
	"rates-api/rates"
	"strconv"
	"strings"
	"time"
  "os"
)

var db map[string]map[int64]float64

// our main function
func main() {
	router := mux.NewRouter()
	router.Use(middleware)

  db = make(map[string]map[int64]float64)

  source := os.Getenv("CSV_PATH")
  if source == "" {
    source = "csv/rates.csv"
  }
  log.Print(source)
	go rates.PopulateRates(source, db)


	router.HandleFunc("/v1/rates/{from}/{to}/{date}", GetRate).Methods("GET")
  router.HandleFunc("/v1/healthcheck", getHealthCheck).Methods("GET")
  log.Print("Starting")
	log.Fatal(http.ListenAndServe(":8080", router))
}

func middleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Add("Content-Type", "application/json")
		next.ServeHTTP(w, r)
	})
}

type Rate struct {
	From string  `json:"from"`
	To   string  `json:"to"`
	Date int64   `json:"date"`
	Rate float64 `json:"rate"`
}

type Message struct {
	Message string `json:"message"`
}

type HealthCheckResponse struct {
  Currencies []string `json:"currencies"`
}

func GetRate(w http.ResponseWriter, r *http.Request) {
	var rate float64
	var dateIndex int64
  log.Print("Getting rate")
	params := mux.Vars(r)
	to := strings.ToUpper(params["to"])
	layout := "20060102"
	dateTime, _ := time.Parse(layout, params["date"])

	for i := 0; i < 4; i++ {
		dateIndex, _ = strconv.ParseInt(dateTime.Format(layout), 10, 0)
		if db[to][dateIndex] != 0.0 {
			rate = db[to][dateIndex]
			break
		}
		dateTime = dateTime.Add(time.Hour * -24)
	}

	if rate != 0.0 {
		json.NewEncoder(w).Encode(&Rate{From: "EUR", To: to, Date: dateIndex, Rate: db[to][dateIndex]})
		return
	}
	
  w.WriteHeader(404)
	json.NewEncoder(w).Encode(Message{Message: "No rate found"})
}

func getHealthCheck(w http.ResponseWriter, r *http.Request) {
  keys := make([]string, len(db))

  i := 0
  for k := range db {
      keys[i] = k
      i++
  }
  json.NewEncoder(w).Encode(HealthCheckResponse{Currencies: keys})

}
