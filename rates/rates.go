package rates

import (
	"bufio"
	"encoding/csv"
	"io"
	"log"
	"os"
	"strconv"
	"strings"
)

var db map[string]map[int64]float64

func PopulateRates(source string, db map[string]map[int64]float64) {
	csvFile, _ := os.Open(source)
	reader := csv.NewReader(bufio.NewReader(csvFile))
	var currencies []string
	for {
		line, error := reader.Read()
		if error == io.EOF {
			break
		}
		if error != nil {
			log.Fatal(error)
		}
		if currencies == nil {
			for i, v := range line {
				if (i != 0) && (len(v) > 0) {
					db[strings.TrimSpace(v)] = make(map[int64]float64)
					currencies = append(currencies, strings.TrimSpace(v))
				}
			}
		} else {
			for i, currency := range currencies {
				date, _ := strconv.ParseInt(strings.Replace(line[0], "-", "", -1), 10, 0)
				rate, _ := strconv.ParseFloat(line[i+1], 32)
				db[currency][date] = rate
			}
		}
	}
	csvFile.Close()
}
